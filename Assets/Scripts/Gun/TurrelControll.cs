using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurrelControll : MonoBehaviour
{
    private TurrelBase turrel;

    public Rigidbody bullet;
    public Transform aim;

    void Start()
    {
        turrel = GetComponent<TurrelBase>();
        Cursor.visible = false;
    }
    
    void Update()
    {
        RaycastHit hit = GameRaycaster.RaycastHitFromCamera();
        if (hit.transform)
        {
            turrel.Rotate(hit.point);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Cursor.visible = true;           
        }

        if (Input.GetMouseButtonUp(0))
        {
            Cursor.visible = false;
            Rigidbody bulletRB;
            bulletRB = Instantiate(bullet, aim.position, aim.rotation) as Rigidbody;
            bulletRB.AddForce(aim.forward * 1500f);
        }
    }
}
