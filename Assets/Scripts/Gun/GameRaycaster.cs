using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRaycaster : MonoBehaviour
{
    private static Camera mainCamera;

    public static RaycastHit RaycastHitFromCamera()
    {
        if (mainCamera == null)
        {
            mainCamera = Camera.main;
        }

        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        Physics.Raycast(ray, out hit);

        return hit;
    }

    public static RaycastHit RaycastHit(Vector3 position, Vector3 direction, float distance = 100f)
    {
        RaycastHit hit;
        Physics.Raycast(position, direction, out hit, distance);

        return hit;
    }
}
