using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurrelBase : MonoBehaviour
{
    public Transform tower;
    public Transform gun;

    public float m_TowerSpeed = 3f;
    public float m_GunSpeed = 3f;

    public void Rotate(Transform target)
    {
        Rotate(target.position);
    }

    public void Rotate(Vector3 target)
    {
        //rotate tower
        Vector3 relativePos = target - tower.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, transform.up);
        tower.rotation = Quaternion.Lerp(tower.rotation, rotation ,m_TowerSpeed * Time.deltaTime);
        Vector3 angles = tower.localEulerAngles;
        angles.x = angles.z = 0;
        tower.localEulerAngles = angles;

        //rotate gun
        relativePos = target - gun.position;
        rotation = Quaternion.LookRotation(relativePos, transform.up);
        gun.rotation = Quaternion.Lerp(gun.rotation, rotation, m_GunSpeed * Time.deltaTime);
        angles = gun.localEulerAngles;
        angles.y = angles.z = 0;
        gun.localEulerAngles = angles;
    }

}
